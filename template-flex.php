<?php
/**
 * Template Name: Flex Content Template
 * Make sure to import custom fields from field_imports/flex-content-import.json
 */
?>
<?php while (have_posts()) : the_post(); ?>
<?php get_template_part('templates/flex-content'); ?>
<?php endwhile; ?>