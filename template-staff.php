<?php
/**
 * Template Name: Staff Template
 */
?>
<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?>>
    <div class="entry-content">
    	<?php the_content(); ?>
    </div><!--./entry-content-->
    <hr/>
    <section class="staff">
	    <?php get_template_part('templates/staff'); ?>
    </section>

  </article>

<?php endwhile; /* end origin loop*/?>	