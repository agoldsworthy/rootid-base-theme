<?php
/**
 * Template Name: Homepage Template
 */
?>
<?php get_template_part('templates/partials/slider'); ?>
<?php while (have_posts()) : the_post(); ?>
  <?php the_content(); ?>
<?php endwhile; ?>