<div class="entry-content">
	<h1><?php single_cat_title(); ?></h1>
	<div class="landing-content">
		<?php echo category_description(); ?>
	</div>
	<?php get_template_part('templates/listing-archive'); ?>
</div>