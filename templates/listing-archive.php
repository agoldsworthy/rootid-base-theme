<?php
if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<article <?php post_class(); ?>>
  <?php if ( has_post_thumbnail() && !is_handheld() ) { ?>
  <div class="thumbnail-container">
    <div class="entry-image">
      <a href="<?php the_permalink(); ?>">
      <?php
        echo get_the_post_thumbnail( $post_id, 'listing-thumbnail' );
      ?>
    </a>
    </div><!--/.entry-image-->
  </div><!--/.thumnail-container-->
    <?php } ?>

  <div class="inner-container">
    <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    <div class="entry-summary">
      <?php echo excerpt(50); ?>
    </div>
    <div><a class="btn btn-default" href="<?php the_permalink(); ?>">LEARN MORE</a></div>

    </div>
</article>

  <div class="clearfix"></div>

  <?php endwhile; else : ?>
    <div class="no-results"><p>There are currently no posts in this section</p></div>
  <?php endif; ?>

  <?php if ($wp_query->max_num_pages > 1) : ?>
  <div class="pagination-container">
  <nav class="post-nav pull-right">
    <?php
      $args = array(
        'mid_size'           => 3,
        'next_text'          => __('More &raquo;'),
      );
      echo paginate_links( $args );
    ?>
  </nav>
</div>
<?php endif; ?>
<?php wp_reset_query(); ?>


<div class="fix"></div>
