<?php

//1) Import field_imports/listing-field-import.json
//2) define image size on line 33 and 37
//3) make sure that Post Type Select for Advanced Custom Fields plug-in is installed & activated
//4) make sure that colorbox js and CSS is loading on the site
//5) select and downlad a colorbox style http://www.jacklmoore.com/colorbox
?>

<div class="staff-inner">
    <div class="section-title"> 
      <h2>Our Team</h2>
    </div>
    <?php 
    //check for the repeater first. We dont want to do a while loop if there are none!
    if(have_rows('staff_members')){ ?>
    <?php $i = 0; ?>
    <div class="staff-content row">
      
      <?php //Loops through the repeater
      while (have_rows('staff_members')) {
        the_row(); $i++;
        
        //fetches the post object field. Assumes this is a single value, if they can select more than one you have to do a foreach loop here like the one in the example above
        $staff_object = get_sub_field('staff_members');
          $name = get_sub_field('staff_name');
          $staff_title = get_sub_field('staff_position');

          //Image 
          $staff_image = get_sub_field('staff_image');
          
          //thumb image
          $staff_image_thumb = "thumbnail";
          $staff_thumb = wp_get_attachment_image_src( $staff_image, $staff_image_thumb );

          //thumb full
          $staff_image_full = "full";
          $staff_full = wp_get_attachment_image_src( $staff_image, $staff_image_full );
        
          $teaser = get_sub_field('staff_bio_teaser');
          $full_bio = get_sub_field('staff_bio_full');
          
          ?>

        <div class="col-xs-12 col-sm-4">
          <div class="staff-item row-<?php echo $i; ?>">
              <div class="bio-image">
                <a class="inline cboxElement" href="#full-<?php echo $i; ?>">
                  <img src="<?php echo $staff_thumb[0] ?>" alt="<?php echo $name; ?> bio image"/>
                </a>
              </div>
              <div class="staff-content">
                <div class="triangle"></div>
                <h3 class="staff-name">
                  <a class="inline cboxElement" href="#full-<?php echo $i; ?>"><?php echo $name ?><br/>
                  <span><?php echo $staff_title; ?></span></a>
                </h3>
                <div class="staff-blurb"><?php echo $teaser ?> <a href="#full-<?php echo $i; ?>" class="inline cboxElement">Read more ></a></div>
              </div>
              <div class="bio-wrap" style="display:none;">
                <div id="full-<?php echo $i; ?>" class="full-bio-inner"><!--colorbox-->
                  <img src="<?php echo $staff_full[0] ?>" alt="$name bio image"/>
                <h3 class="staff-name">
                  <?php echo $name ?><br/>
                  <span><?php echo $staff_title; ?></span>
                </h3>
                <div class="full-bio">
                  <?php echo $full_bio;?>
                </div>
              </div><!--/.full-bio-inner-->
            </div><!--/.bio-wrap-->
        </div><!--/.staff-content-->
        </div>
      <?php } 
      
    } 
    ?>
        </div><!-- /.services-content-->  
<script>
(function($) 
          {
            $(document).ready(function() {
              $(".inline").colorbox({inline:true, width:"95%"});
            });
          })(jQuery);
</script>          

</div><!-- /.services-inner-->
