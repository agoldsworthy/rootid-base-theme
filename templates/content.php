<article <?php post_class(); ?>>
  <header>
    <?php get_template_part('templates/partials/entry-meta'); ?>
  </header>
  <div class="entry-summary">
    <?php the_excerpt(); ?>
  </div>
</article>
