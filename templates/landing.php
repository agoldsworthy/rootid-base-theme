<?php 

  // 1) Import field_imports/landing-field-import.json
  // 2) Define image size on line 10

  global $post;

  $relationship = get_field('add_content');
  if ($relationship != '') {
  foreach ($relationship as $postID) {
    $title = get_the_title( $postID );
    $thumbnail = get_the_post_thumbnail( $postID, 'thumbnail' );
    $link = get_permalink( $postID );
    ?>
    <article <?php post_class(); ?>>
  <?php if( ($thumbnail != '') && (!is_handheld()) ) { ?>
  <div class="thumbnail-container">
    <div class="entry-image">
      <a href="<?php echo $link ?>" title="link to <?php echo $title ?>">
      <?php
        echo $thumbnail;
      ?>
    </a>
    </div>
  </div>
  <?php } ?>
  <div class="inner-container">
    <h2 class="entry-title"><a href="<?php echo $link ?>" title="link to <?php echo $title ?>"><?php echo $title; ?></a></h2>
    <div><a class="btn btn-default" href="<?php echo $link ?>" title="link to <?php echo $title ?>">LEARN MORE</a></div>

    </div>
</article>

  <div class="clearfix"></div>

<?php } //endforeach 
  } //endif
wp_reset_postdata(); //Restore original Post Data ?>
<div class="fix"></div>