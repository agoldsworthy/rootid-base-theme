<?php 

// Begin Looping Through FLexible Content Rows
if( have_rows('component') ){

    while ( have_rows('component') ) : the_row();


	// Header Slider
	if( get_row_layout() == 'slider' ){
	  echo '<div id="header-slider" class="owl-slider owl-carousel">';

	  // check if the repeater field has rows of data
	  if( have_rows('slide') ){
	     // loop through the rows of data
	    while ( have_rows('slide') ) : the_row();  
	    $image_id = get_sub_field('image');
	    
	    if ( is_mobile() ) {
	      $image_size = 'thumbnail';
	    } else {
	      $image_size = 'full';
	    }

	    $image_array = wp_get_attachment_image_src($image_id, $image_size);
	    $image_url = $image_array[0];


	    echo '<div class="item">';
	    echo '<div class="header-wrap"><img src="';
	    echo $image_url;
	    echo '"/>';
	  	echo '<div class="header-title">';
	  	echo '<h2><a href="'.get_sub_field('link').'">'.get_sub_field('title').'</a></h2>';
	  	echo '<div class="sub-title"><a href="'.get_sub_field('link').'">'.get_sub_field('teaser').'</a></div>';
	    echo '</div></div></div>';
	    endwhile;
	  }
	  echo '</div>';
	  ?>

	 <script>
	 	(function($) 
	 	{
	 		$(document).ready(function() {
	 			$('.owl-slider').owlCarousel({
	 				items: 1,
	 				loop: true,
	 				nav: true,
	 				navText: ['<i class="fa fa-chevron-right"></i>','<i class="fa fa-chevron-left"></i>']
	 			});
	 		});
	 	})(jQuery);
	 	
	 </script>

	<?php } 
	// End Header Slider

	// Highlights
	// IMPORTANT: Depending on how many across you want these items, 
	// you need to adjust the bootstrap scaffolding on the items

	if( get_row_layout() == 'features' ){
	  echo '<div class="features"><div class="row">';

	  // check if the repeater field has rows of data
	  if( have_rows('features') ){
	     // loop through the rows of data
	    while ( have_rows('features') ) : the_row();  
	    $highlight_link = get_sub_field('link');
	    $highlight_title = get_sub_field('title');
	    
	    $highlight_id = get_sub_field('image');
	    $highlight_size = 'thumbnail';

	    $highlight_array = wp_get_attachment_image_src($highlight_id, $highlight_size);
	    $highlight_url = $highlight_array[0];

	    echo '<div class="col-xs-12 col-sm-4 feature-item">';
	    echo '<div class="feature-image"><img src="';
	    echo $highlight_url;
	    echo '"/>';
	    echo '<div class="feature-title"><h3><a href="'.$highlight_link.'">';
	    echo $highlight_title;
	    echo '</a></h3></div></div></div>';
	    endwhile;
	  }
	  echo '</div></div>';

	}    
	// End Higlight

	// Body Content

	if( get_row_layout() == 'body_content' ){
	  echo '<div class="body-content">';
	  the_sub_field('body_content');
	  echo '</div>';
	}    
	// End body

	// Image Left w/ Content

	if( get_row_layout() == 'content_image_left' ){
    
	    $image_left_id = get_sub_field('image_left');

	    //change image size to suite design
	    $image_left_size = 'thumbnail';

	    $image_left_array = wp_get_attachment_image_src($image_left_id, $image_left_size);
	    $image_left_url = $image_left_array[0];

	  echo '<div class="image-left"><div class="row">';
	  echo '<div class="col-xs-12 col-sm-6 col-md-3">';
	  echo '<img src="';
	  echo $image_left_url;
	  echo '"/></div>';
	  echo '<div class="col-xs-12 col-sm-6 col-md-9">';
	  the_sub_field('image_left_content');
	  echo '</div></div></div>';
	}    
	// End Image Left w/ Content

	// Image Left w/ Content
	if( get_row_layout() == 'content_image_right' ){
    
	    $image_right_id = get_sub_field('image_right');

	    //change image size to suite design
	    $image_right_size = 'thumbnail';

	    $image_right_array = wp_get_attachment_image_src($image_right_id, $image_right_size);
	    $image_right_url = $image_right_array[0];

	  echo '<div class="image-left"><div class="row">';
	  echo '<div class="col-xs-12 col-sm-6 col-md-9">';
	  the_sub_field('image_right_content');
	  echo '</div>';
	  echo '<div class="col-xs-12 col-sm-6 col-md-3">';
	  echo '<img src="';
	  echo $image_right_url;
	  echo '"/></div>';
	  echo '</div></div>';
	}    
	// End Image Left w/ Content


	// Carousel
	if( get_row_layout() == 'carousel' ){
	  echo '<div id="owl-carousel" class="owl-carousel">';

	  // check if the repeater field has rows of data
	  if( have_rows('carousel') ){
	     // loop through the rows of data
	    while ( have_rows('carousel') ) : the_row();  
	    $image_id = get_sub_field('image');
	    
	    if ( is_mobile() ) {
	      $image_size = 'thumbnail';
	    } else {
	      $image_size = 'full';
	    }

	    $image_array = wp_get_attachment_image_src($image_id, $image_size);
	    $image_url = $image_array[0];


	    echo '<div class="item">';
	    echo '<div class="carousel-image"><img src="';
	    echo $image_url;
	    echo '"/></div>';
	  	echo '<div class="carousel-title">';
	  	echo '<h3><a href="'.get_sub_field('link').'">'.get_sub_field('title').'</a></h3>';
	    echo '</div></div>';
	    endwhile;
	  }
	  echo '</div>';
	  ?>

	 <script>
	 	(function($) 
	 	{
	 		$(document).ready(function() {
	 			$('#owl-carousel').owlCarousel({
	 				items: 3,
	 				loop: true,
	 				nav: true,
	 				margin: 10,
	 				navText: ['<i class="fa fa-chevron-right"></i>','<i class="fa fa-chevron-left"></i>'],
	 				responsiveRefreshRate: 200,
	 				responsive : {
					    // breakpoint from 0 up
					    0 : {
					        items : 1
					    },
					    // breakpoint from 480 up
					    480 : {
					        items : 2
					    },
					    // breakpoint from 768 up
					    768 : {
					        items : 3
					    }
					}

	 			});
	 		});
	 	})(jQuery);
	 </script>

	<?php } 



endwhile;
    // End Content Blocks
}
else {

    // no layouts found

};
?>
