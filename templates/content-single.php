<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?>>
    <div class="entry-content">
      <header>
      <?php //get_template_part('templates/partials/entry-meta'); ?>
    </header>
      <?php the_content(); ?>
      <footer>
      <?php wp_link_pages(array('before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>')); ?>
      </footer>
    </div>

    <?php //comments_template('templates/partials/comments.php'); ?>
  </article>
<?php endwhile; ?>