<header class="banner" role="banner">
  <div class="banner-inner">
    <div class="row">
      <button type="button" class="navbar-toggle visible-xs visible-sm" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <div class="col-xs-12 col-md-6 col-lg-3">
        <h1 class="site-title"><a href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?><span> - <?php bloginfo('description'); ?></span></a></h1>
      </div>
      <div class="hidden-xs hidden-sm col-md-6 col-lg-9">
        <?php wp_nav_menu( array( 'theme_location' => 'primary_navigation', 'menu_class' => 'nav nav-pills') ); ?>
      </div>
    </div>
  </div>
</header>
<div class="clearfix"></div>
