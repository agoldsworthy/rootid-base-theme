<?php use Roots\Sage\Titles; ?>
	<? if ( has_post_thumbnail( ) ) {
			the_post_thumbnail();
		} ?>
  <h1><?= Titles\title(); ?></h1>
