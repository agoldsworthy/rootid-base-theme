<footer id="footer" class="content-info" role="contentinfo">
	<div class="container">
		<div class="col-xs-12 col-lg-6 footer-left">
			<nav role="navigation" class="navbar hidden-xs hidden-sm">
				<?php
				if (has_nav_menu('footer_menu')) :
				wp_nav_menu(array(
				              'menu'        => 'Footer Menu', 
				              'walker'      => new Roots_Nav_Walker, 
				              'menu_class'  => 'nav nav-pills',
				              'link_before' => '<span>',
				              'link_after'  => '</span>')
				);
				endif;
				?>
			</nav>
			<?php get_template_part('lib/socialbar'); ?>
			<?php dynamic_sidebar('sidebar-copyright'); ?>
		</div>
		<div class="col-xs-12 col-lg-6 footer-right">
			<?php dynamic_sidebar('sidebar-footer'); ?>
		</div>
	</div>
</footer>
