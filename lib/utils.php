<?php

namespace Roots\Sage\Utils;

/**
 * Tell WordPress to use searchform.php from the templates/ directory
 */
function get_search_form() {
  $form = '';
  locate_template('/templates/searchform.php', true, false);
  return $form;
}
add_filter('get_search_form', __NAMESPACE__ . '\\get_search_form');

/**
 * Registers Nav Menus
 */
register_nav_menus( array(
	'footer_menu' => 'Footer Menu',
	'primary_navigation' => 'Primary Navigation',
	'mobile_navigation' => 'Mobile Navigation',
) );

/**
 * Add image sizes
 * Images should take the name of the dimensions.  
 * For instance, images that are cropped to 400px by 400px should be named 400x400
 * add_image_size( '400x400', 400, 480, true );
 */
