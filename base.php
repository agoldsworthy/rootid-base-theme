<?php

use Roots\Sage\Config;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
  <?php get_template_part('templates/partials/head'); ?>
  <body <?php body_class(); ?>>
    <?php get_template_part('templates/partials/off-canvas'); ?>
    <!--[if lt IE 9]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php
      do_action('get_header');
      get_template_part('templates/partials/header');
    ?>
    <div class="wrap" role="document">
      <div class="page-header container-fluid">
        <?php get_template_part('templates/partials/page-header'); ?>
      </div>
      <div class="content container-fluid">
        <div class="row">
        <?php if ( is_active_sidebar( 'sidebar-primary' ) ) { ?>
          <main class="main col-md-9" role="main">
        <?php } else { ?>
          <main class="main col-xs-12" role="main">
        <?php } ?>
          <?php include Wrapper\template_path(); ?>
        </main><!-- /.main -->
        <?php if ( is_active_sidebar( 'sidebar-primary' ) ) { ?>
          <aside class="col-md-3 sidebar" role="complementary">
            <?php include Wrapper\sidebar_path(); ?>
          </aside><!-- /.sidebar -->
        <?php } ?>
        </div><!--.row -->
      </div><!-- /.content -->
    </div><!-- /.wrap -->
    <?php
      do_action('get_footer');
      get_template_part('templates/partials/footer');
      wp_footer();
    ?>
  </body>
</html>
